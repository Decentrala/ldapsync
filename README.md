# LGit (pronounced "legit")

python library that allows to use LDAP user database, based on luser project with git-like functions

Examples:

Following script copies changes made on 192.168.1.13 LDAP server to the 192.168.1.21 server

```
import lgit

server1 = lgit.logserver("192.168.1.13", "cn=admin,dc=example,dc=com", "adminpass", "ou=Users,dc=example,dc=com")
server2 = lgit.logserver("192.168.1.21", "ou=bob,ou=Users,dc=example,dc=com", "pasSw0rd", "ou=Users,dc=example,dc=com")

server1.pull(server2)

```

Following script pulls all user changes from both servers to each other

```
import lgit

server1 = lgit.logserver("192.168.1.13", "cn=admin,dc=example,dc=com", "adminpass", "ou=Users,dc=example,dc=com")
server2 = lgit.logserver("192.168.1.21", "ou=bob,ou=Users,dc=example,dc=com", "pasSw0rd", "ou=Users,dc=example,dc=com")

lg = lgit.lgit()
lg.sync()

```
